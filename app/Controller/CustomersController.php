<?php

App::uses('CakeEmail', 'Network/Email');

class CustomersController extends AppController
{


	public $components = array('Auth');

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow('add', 'logout');
	}


	public function index()
	{
		$this->Customer->recursive = 0;
		$this->set('customers', $this->paginate());

		$options = array(
			'order' => array('Customer.name'),
			'limit' => 25
		);

		$this->paginate = $options;
		// Roda a consulta, já trazendo os resultados paginados
		$customers = $this->paginate('Customer');
		// Envia os dados pra view
		$this->set('customers', $customers);


	}

	public function view($id = null)
	{
		$this->Customer->id = $id;
		if (!$this->Customer->exists()) {
			throw new NotFoundException(__('Cliente Inv&aacute;lido'));
		}
		$this->set('customer', $this->Customer->read(null, $id));
	}

	public function add()
	{
		//debug($this->request);
		if ($this->request->is('post')) {
			$this->request->data['Customer']['user_id'] = $this->Auth->user('id');
			if ($this->Customer->save($this->request->data)) {
				$this->Session->setFlash(__('O cliente foi salvo'));
				$Email = new CakeEmail('smtp');
				$Email->to('giucabral@gmail.com');
				$Email->subject('Cliente Novo!');
				$Email->send('Cadastro do Cliente ' . $this->request->data['Customer']['name'] . ' verifique!');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O cliente n&atilde;o foi salvo. Por favor, tente novamente.'));
			}
		}
	}

	public function edit($id = null)
	{
		//debug($this->request);
		$this->Customer->id = $id;

		if (!$this->Customer->exists()) {
			throw new NotFoundException(__('Cliente Inválido'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Customer->save($this->request->data)) {
				$Email = new CakeEmail('smtp');
				$Email->to('giucabral@gmail.com');
				$Email->subject('Cliente Alterado!');
				$Email->send('Os dados do Cliente ' . $this->request->data['Customer']['name'] . ' foram alterados!');
				$this->Session->setFlash(__('O cliente foi salvo!'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O cliente não foi salvo. Por favor, tente novamente.'));
			}
		} else {
			$this->request->data = $this->Customer->read(null, $id);
			//unset($this->request->data['User']['password']);

			if ($this->Auth->user('role') === 'vendedor') {
				if ($this->Auth->user('id') <> $this->request->data['Customer']['user_id']) {
					$this->Session->setFlash(__('Voc&ecirc; n&atilde;o tem permiss&atilde;o para editar este cliente!'));
					$this->redirect(array('action' => 'index'));
					//debug($this->request->data);
					//debug($this->Auth->user('id'));
				}
			}
		}
	}

	public function delete($id = null)
	{
		$this->request->data = $this->Customer->read(null, $id);
		//debug($this->request->data);
		if ($this->Auth->user('role') === 'vendedor') {
			if ($this->Auth->user('id') <> $this->request->data['Customer']['user_id']) {
				$this->Session->setFlash(__('Voc&ecirc; n&atilde; tem permiss&atilde;o para apagar este cliente!'));
				$this->redirect(array('action' => 'index'));
				//debug($this->request->data);
				//debug($this->Auth->user('id'));
			}
		}


		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Customer->id = $id;
		if (!$this->Customer->exists()) {
			throw new NotFoundException(__('Cliente Inválido'));
		}
		if ($this->Customer->delete()) {
			$this->Session->setFlash(__('Cliente Deletado'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('O cliente não foi deletado'));
		$this->redirect(array('action' => 'index'));
	}

	public function isAuthorized($user)
	{
		if (parent::isAuthorized($user)) {
			if ($this->action === 'add') {
				//todos os usuários podem criar posts
				return true;
			}
			if (in_array($this->action, array('edit', 'delete'))) {
				$customerId = (int)$this->request->params['pass'][0];
				debug($customerId);
				return $this->Custormer->isOwnedBy($customerId, $user['id']);
			}
		}
		return false;
	}
}


?>
