<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController
{

	public function index()
	{
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

	public function view($id = null)
	{
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Usu&aacute;rio Inv&aacute;lido'));
		}
		$this->set('user', $this->User->read(null, $id));
	}

	public function add()
	{
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('O usu&aacute;rio foi salvo'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O usu&aacute;rio n&atilde;o foi salvo. Por favor, tente novamente.'));
			}
		}
	}

	public function edit($id = null)
	{
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Usu&aacute;rio Inv&aacute;lido'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('O usu&aacute;rio foi salvo!'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('O usu&aacute;rio não foi salvo. Por favor, tente novamente.'));
			}
		} else {
			$this->request->data = $this->User->read(null, $id);
			unset($this->request->data['User']['password']);
		}
	}

	public function delete($id = null)
	{
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Usu&aacute;rio Inv&aacute;lido'));
		}
		if ($this->User->delete()) {
			$this->Session->setFlash(__('Usu&aacute;rio Deletado'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('O usu&aacute;rio n&atilde;o foi deletado'));
		$this->redirect(array('action' => 'index'));
	}

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow(array('login', 'index'));
	}


	public function login()
	{
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$this->redirect($this->Auth->redirectUrl());
			} else {
				$this->Session->setFlash(__('Usu&aacute;rio ou password inv&aacute;lido, tente outra vez!'));
			}
		}
		//return $this->redirect($this->Auth->redirect());
	}

	public function logout()
	{
		$this->redirect($this->Auth->logout());
	}

}
