<div class="users form">
<?php echo $this->Session->flash('auth'); ?>
<?php echo $this->Form->create('User', array(
	'inputDefaults' => array(
		'div' => 'form-group',
		'wrapInput' => false,
		'class' => 'form-control'
	),
	'class' => 'well'
));?>


    <fieldset>
        <legend><?php echo __('Por favor, insira seu usu&aacute;rio e password'); ?></legend>
        <?php echo $this->Form->input('username', array('label' => 'Usu&aacute;rio','class' => 'form-control'));
        echo $this->Form->input('password', array('label' => 'Senha','class' => 'form-control'));
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Logar', array( '<span class="form-control"></span>')));?>
</div>


