<!-- app/View/Users/edit.ctp -->
<div class="users form">
<?php echo $this->Form->create('User', array('action' => 'edit',
												'inputDefaults' => array(
													'div' => 'form-group',
													'wrapInput' => false,
													'class' => 'form-control'
												),
												'class' => 'well'
));?>
    <fieldset>
        <legend><?php echo __('Editar Usu&aacute;rio'); ?></legend>
<?php 
		echo $this->Form->input('username', array('label' => 'Usu&aacute;rio','class' => 'form-control'));
        echo $this->Form->input('password', array('label' => 'Password','class' => 'form-control'));
		echo $this->Form->input('name', array('label' => 'Nome','class' => 'form-control'));
        echo $this->Form->input('role', array('label' => 'Perfil',
            'options' => array('gerente' => 'Gerente', 'vendedor' => 'Vendedor'),
            'class' => 'form-control'
			)
		);
		echo $this->Form->input('id', array('type' => 'hidden'));
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Salvar', array( 'class' => 'form-control')));?>
</div>