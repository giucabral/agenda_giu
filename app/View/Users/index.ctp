<!-- app/View/Users/index.ctp -->
<h1>Usu&aacute;rios</h1>
<p><?php echo $this->Html->link("Adicionar Usuarios", array('controller' => 'users', 'action' => 'add')); ?></p>
<table class="table table-bordered">
    <tr>
        <th>Id</th>
        <th>Usu&aacute;rio</th>
		<th>Password</th>
		<th>Nome</th>
		<th>Perfil</th>
		<th>Actions</th>
        <th>Data de Cria&ccedil;&atilde;o</th>
    </tr>

<?php		
	foreach ($users as $user): 
?>
<tr>
	<td><?php echo $user['User']['id']; ?></td>
	<td><?php echo $this->Html->link($user['User']['username'], array('controller' => 'users', 'action' => 'view', $user['User']['id'])); ?> </td>
	<td><?php echo $user['User']['password']; ?> </td>
	<td><?php echo $user['User']['name']; ?> </td>
	<td><?php echo $user['User']['role']; ?> </td>
	<td><?php echo $this->Form->postLink(
		'Delete', 
		array('action' => 'Delete', $user['User']['id']),
		array('confirm' => 'Deseja realmente apagar o usu&aacute;rio?')
		)?>
		<?php echo $this->Html->link(
		'Editar',
		array('action' => 'edit', $user['User']['id'])
		); ?>
	</td>
	<td><?php echo $user['User']['created']; ?></td>
</tr>
<?php 
	endforeach; 
?>	
</table>