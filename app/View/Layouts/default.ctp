<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Projeto Agenda de Clientes - Solutudo';
//$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<h1> Agenda de Clientes </h1>
		<?php echo $this->fetch('title'); ?>
	</title>
	<?php

	echo $this->Html->meta('icon');

	echo $this->Html->css('bootstrap');
	echo $this->Html->css('bootstrap.min');
	echo $this->Html->css('bootstrap-theme');
	echo $this->Html->css('bootstrap-theme.min');

	echo $this->Html->script('bootstrap');
	echo $this->Html->script('bootstrap.min');
	echo $this->Html->script('npm');

	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	?>
</head>
<body>
<div class="container" id="container">
	<div class="container" id="header">
		<h1><?php echo $this->Html->link($cakeDescription, 'www.dotneo.com.br'); ?></h1>

		<div class="btn-group btn-group-justified" role="group" aria-label="...">
			<div class="btn-group" role="group">
				<?php echo $this->Html->link('Clientes', ['controller' => 'customers'], ['class' => 'btn btn-default']); ?>
			</div>
			<div class="btn-group" role="group">
				<?php echo $this->Html->link('Vendedores', ['controller' => 'users'], ['class' => 'btn btn-default']); ?>
			</div>
			<div class="btn-group" role="group">
				<?php echo $this->Html->link('Logout', ['controller' => 'users', 'action' => 'logout'], ['class' => 'btn btn-default']); ?>
			</div>
		</div>


	</div>
	<div class="container" id="content">

		<?php echo $this->Session->flash(); ?>

		<?php echo $this->fetch('content'); ?>
	</div>
	<div id="footer">
	</div>
</div>
</body>
</html>
