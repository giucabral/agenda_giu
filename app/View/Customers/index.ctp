<!-- app/View/Customer/index.ctp -->
<h1>Clientes</h1>
<p><?php echo $this->Html->link('Adicionar Clientes', array('controller' => 'customers', 'action' => 'add')); ?></p>
<table class="table table-bordered">
    <tr>
        <th>Id</th>
        <th>Nome</th>
		<th>Telefone</th>
		<th>E-mail</th>
		<th>Endere&ccedil;o</th>
		<th>Anivers&aacute;rio</th>
		<th>Actions</th>
        <th>Data de Cria&ccedil;&atilde;o</th>
    </tr>

<?php		
	foreach ($customers as $customer): 
?>
<tr>
	<td><?php echo $customer['Customer']['id']; ?></td>
	<td><?php echo $this->Html->link($customer['Customer']['name'], array('controller' => 'customers', 'action' => 'view', $customer['Customer']['id'])); ?> </td>
	<td><?php echo $customer['Customer']['phone']; ?> </td>
	<td><?php echo $customer['Customer']['email']; ?> </td>
	<td><?php echo $customer['Customer']['addres']; ?> </td>
	<td><?php echo $customer['Customer']['birth']; ?> </td>
	<td><?php echo $this->Form->postLink(
		'Delete', 
		array('action' => 'Delete', $customer['Customer']['id']),
		array('confirm' => 'Deseja realmente apagar o cliente?')
		)?>
		<?php echo $this->Html->link(
		'Editar',
		array('action' => 'edit', $customer['Customer']['id'])
		); ?>
	</td>
	<td><?php echo $customer['Customer']['created']; ?></td>
</tr>
<?php 
	endforeach;
	
	
	echo $this->Paginator->prev(' <= ', null, null, array('class' => 'desabilitado'));
	echo $this->Paginator->numbers();
	echo $this->Paginator->next(' => ', null, null, array('class' => 'desabilitado'));
	 
?>	
</table>
<?php
	echo $this->Paginator->prev(' <= ', null, null, array('class' => 'desabilitado'));
	echo $this->Paginator->numbers();
	echo $this->Paginator->next(' => ', null, null, array('class' => 'desabilitado'));
?>