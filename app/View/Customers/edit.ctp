<!-- app/View/Customer/edit.ctp -->
<div class="customers form">
<?php echo $this->Form->create('Customer', array('action' => 'edit',
												'inputDefaults' => array(
													'div' => 'form-group',
													'wrapInput' => false,
													'class' => 'form-control'
												),
												'class' => 'well'
));?>
    <fieldset>
        <legend><?php echo __('Editar Cliente'); ?></legend>
<?php 
		echo $this->Form->input('name', array('label' => 'Nome', 'class' => 'form-control'));
        echo $this->Form->input('phone', array('label' => 'Telefone', 'placeholder' => '(00) 00000-0000', 'class' => 'form-control'));
		echo $this->Form->input('email', array('label' => 'E-mail', 'class' => 'form-control'));
		echo $this->Form->input('addres', array('label' => 'Endere&ccedil;o', 'class' => 'form-control'));
		echo $this->Form->input('birth', array('label' => 'Nascimento', 'class' => 'form-control'));
		echo $this->Form->input('id', array('type' => 'hidden'));
    ?>
    </fieldset>
<?php echo $this->Form->end(__('Salvar', array( 'class' => 'form-control')));?>
</div>


<input class="form-control" type="text" placeholder="Default input">