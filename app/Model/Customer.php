<?php

class Customer extends AppModel
{
	public $name = 'Customer';

	public $validate = array(
		'name' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'O campo Nome não pode ficar em branco!'
			)
		),
		'phone' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'O campo Telefone não pode ficar em branco!'
			)
		),
		'email' => array(
			'required' => array(
				//'rule' => array('notEmpty'),
				'message' => 'O campo e-mail não pode ficar em branco!',
				'rule' => array('email', true),
				'message' => 'Por favor, insira um e-mail válido.'
			)
		),
		'addres' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'O campo endereço não pode ficar em branco!'
			)
		),
		'birth' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'O campo data de nascimento não pode ficar em branco!'
			)
		)
	);

	public function isOwnedBy($customer, $user)
	{
		return $this->field('id', array('id' => $customer, 'user_id' => $user)) === $customer;
	}
}

?>
