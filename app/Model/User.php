<?php
App::uses('Security', 'Utility');

class User extends AppModel
{
	public $name = 'User';
	public $validate = array(
		'username' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'O campo Username não pode ficar em branco!'
			)
		),
		'password' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'O campo Password não pode ficar em branco!'
			)
		),
		'name' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'message' => 'O campo Nome não pode ficar em branco!'
			)
		),
		'role' => array(
			'valid' => array(
				'rule' => array('inList', array('gerente', 'vendedor')),
				'message' => 'Por favor escolha uma função',
				'allowEmpty' => false
			)
		)
	);

	public function beforeSave($options = array())
	{
		if (isset($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = Security::hash($this->data[$this->alias]['password'], null, true);
		}
		return true;
	}
}
